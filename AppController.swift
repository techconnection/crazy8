//
//  AppController.swift
//  CHIP8
//
//  Created by Derek Smith on 9/12/15.
//  Copyright (c) 2015 Tech Connection. All rights reserved.
//

import Cocoa

class AppController: NSViewController {

    
    
    override func viewDidLoad() {
        if #available(OSX 10.10, *) {
            super.viewDidLoad()
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func loadChipFile(_ file : URL){
        let chipBytes = try? Data(contentsOf: file)
        NSLog("\(chipBytes!.count)")
        Chip8.sharedInstance.loadProgram(chipBytes!)
    }
    
    
    
    func selectFileToOpen() {
        
        let openPanel = NSOpenPanel();
        openPanel.allowsMultipleSelection = false;
        openPanel.canChooseDirectories = false;
        openPanel.canCreateDirectories = false;
        openPanel.canChooseFiles = true;
        let i = openPanel.runModal();
        if(i.rawValue == NSApplication.ModalResponse.OK.rawValue){
            print(openPanel.url!.absoluteString, terminator: "");
            loadChipFile(openPanel.url!)
        }
        
    }
    
    
    
    
    @IBAction func openDocument(_ _sender: AnyObject?) {
        selectFileToOpen()
    }
}
