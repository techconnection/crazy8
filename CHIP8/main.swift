//
//  main.swift
//  CHIP8
//
//  Created by Derek Smith on 9/21/14.
//  Copyright (c) 2014 Tech Connection. All rights reserved.
//

import Cocoa

print(NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv))
