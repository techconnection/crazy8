//
//  AppDelegate.swift
//  CHIP8
//
//  Created by Derek Smith on 9/21/14.
//  Copyright (c) 2014 Tech Connection. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
                            
    @IBOutlet var window: NSWindow?
    @IBOutlet var chip8View : Chip8View?
    //var mainController : AppController!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        //mainController = AppController(nibName: "AppController", bundle: nil)
        //window!.contentView.addSubview(mainController.view)
        //mainController.view.frame = (window?.contentView as! NSView).bounds
        //NSLog("App loaded")
        if let chip8 = self.chip8View {
            let _ = Timer.scheduledTimer(timeInterval: 1.0/60, target: chip8, selector: #selector(Chip8View.update), userInfo: nil, repeats: true)
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }
}
