//
//  Chip8View.swift
//  CHIP8
//
//  Created by Derek Smith on 9/6/15.
//  Copyright (c) 2015 Tech Connection. All rights reserved.
//

import Cocoa


class Chip8View: NSView {

    func plotPixel(_ x: CGFloat, y: CGFloat, pen:CGFloat) {
        let rect = NSMakeRect(x, self.frame.height-y, pen-1, pen-1);
        let bPath:NSBezierPath = NSBezierPath(rect: rect)
        //let fillColor = NSColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //let fillColor = NSColor(red: 99/255, green: 154/255, blue: 61/255, alpha:1.0)
        let strokeColor = NSColor(red:0.3, green: 0.3, blue:0.3, alpha:1.0)
        strokeColor.set()
        bPath.fill()
        //strokeColor.set()
        //bPath.stroke()
    }
    
    func fillRect(_ rect: NSRect){
        let bPath:NSBezierPath = NSBezierPath(rect: rect)
        //let fillColor = NSColor(red: 0.1, green: 0.0, blue: 0.1, alpha: 1.0)
        let fillColor = NSColor(red: 99/255, green: 154/255, blue: 61/255, alpha:1.0)
        fillColor.set()
        bPath.fill()
    }
    
    override func draw(_ dirtyRect: NSRect) {
        var high = 31
        var wide = 63
        var pixel = 0
        let hiDef = Chip8.sharedInstance.resolution
        super.draw(dirtyRect)
        
        fillRect(dirtyRect)
        if hiDef==1 { high = 63 } else { high = 31 }
        if hiDef==1 { wide = 127 } else { wide = 63 }
        for x in 0...wide {
            for y in 0...high {
                pixel = Chip8.sharedInstance.getChipPixel(x, y)
                if pixel != 0 {
                    //NSLog("Found pixel at:\(x),\(y)")
                    if hiDef==1 {
                        plotPixel(CGFloat((x)*4),y:CGFloat((y+1)*4),pen:CGFloat(4))
                    } else {
                        plotPixel(CGFloat((x)*8),y:CGFloat((y+1)*8),pen:CGFloat(8))
                    }
                }
            }
        }
    }
    /*
    0,0
    ....
    ....
    ....
    ....
    0,3   3,3
    */
    
    @objc func update() {
        Chip8.sharedInstance.fetchAndExecute()
        self.needsDisplay = true
    }
    
    
}
