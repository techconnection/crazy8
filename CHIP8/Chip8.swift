//
//  Chip8.swift
//  CHIP8
//
//  Created by Derek Smith on 9/11/15.
//  Copyright (c) 2015 Tech Connection. All rights reserved.
//

import Foundation
import CoreGraphics

let kVK_ANSI_1 : CGKeyCode                   = 0x12
let kVK_ANSI_2 : CGKeyCode                   = 0x13
let kVK_ANSI_3 : CGKeyCode                   = 0x14
let kVK_ANSI_4 : CGKeyCode                   = 0x15

let kVK_ANSI_Q : CGKeyCode                    = 0x0C
let kVK_ANSI_W : CGKeyCode                    = 0x0D
let kVK_ANSI_E : CGKeyCode                    = 0x0E
let kVK_ANSI_R : CGKeyCode                    = 0x0F

let kVK_ANSI_A  : CGKeyCode                   = 0x00
let kVK_ANSI_S  : CGKeyCode                   = 0x01
let kVK_ANSI_D  : CGKeyCode                   = 0x02
let kVK_ANSI_F  : CGKeyCode                   = 0x03

let kVK_ANSI_Z  : CGKeyCode                  = 0x06
let kVK_ANSI_X  : CGKeyCode                  = 0x07
let kVK_ANSI_C  : CGKeyCode                  = 0x08
let kVK_ANSI_V  : CGKeyCode                  = 0x09



class Chip8 {
    static let sharedInstance = Chip8()
    
    var resolution = 0
    var IndexRegister = 0
    var ProgramCounter  = 0
    var StackPointer = 0
    var DelayRegister  = 0
    var SoundRegister = 0
    var ChipRegister = [Int](repeating: 0, count: 16)
    var InterruptPeriod = 0                             // number of opcodes per
    var chip8_keys = [Int](repeating: 0, count: 16)         // if 1, key is held down
    var Chip8VRAM = [Int](repeating: 0, count: 8192)        //(127,63) // 0xff if pixel is set,
    //var dirtyBits = [Boolean](count:8192, repeatedValue:0)                     //(127,63) : boolean
    var Chip8RAM = [UInt8](repeating: 0, count: 4096)                            // : Unsigned Byte// machine memory. program
    var EmulationRunning = 0                                    // if 0, emulation stops
    var Chip8KeyDown = 0
    var Chip8Sprite = [UInt8](repeating: 0, count: 80)
    var debug = false
    
    init() {
        resetEmulation()
    }
    
    func dsLog(_ s:String) {
        if debug {
            NSLog(s)
        }
    }
    
    func loadProgram(_ chip8Bytes:Data) {
        resetEmulation()
        (chip8Bytes as NSData).getBytes(&Chip8RAM[512], length:chip8Bytes.count)
        EmulationRunning = 1
    }
    
    func write_mem(_ addr:Int,_ value:UInt8) {
        Chip8RAM[addr] = value
    }
    
    
    func resetEmulation(){
        //this is the default font set
        Chip8Sprite = [
            0xf0,0x90,0x90,0x90,0xf0, //0
            0x20,0x60,0x20,0x20,0x70, //1
            0xf0,0x10,0xf0,0x80,0xf0, //2
            0xf0,0x10,0xf0,0x10,0xf0, //3
            0x90,0x90,0xf0,0x10,0x10, //4
            0xf0,0x80,0xf0,0x10,0xf0, //5
            0xf0,0x80,0xf0,0x90,0xf0, //6
            0xf0,0x10,0x20,0x40,0x40, //7
            0xf0,0x90,0xf0,0x90,0xf0, //8
            0xf0,0x90,0xf0,0x10,0xf0, //9
            0xf0,0x90,0xf0,0x90,0x90, //A
            0xe0,0x90,0xe0,0x90,0xe0, //B
            0xf0,0x80,0x80,0x80,0xf0, //C
            0xe0,0x90,0x90,0x90,0xe0, //D
            0xf0,0x80,0xf0,0x80,0xf0, //E
            0xf0,0x80,0xf0,0x80,0x80  //F
            ]
        for i in 0..<80
        {
            write_mem (i,Chip8Sprite[i])
        }
        
        for column in 0...15 {
            ChipRegister[column] = 0
            chip8_keys[column] = 0
        }
        for column in 0...8191 {
            Chip8VRAM[column] = 0
            //dirtyBits[column] = 0
        }
        
        Chip8KeyDown = 0
        DelayRegister = 0
        SoundRegister = 0
        IndexRegister = 0
        StackPointer = 0x1e0
        ProgramCounter = 512
        InterruptPeriod = 15
        resolution = 0
        //stop any sound
        EmulationRunning = 0
        
        
        /*
        setChipPixel(127, 0)
        setChipPixel(0, 0)
        setChipPixel(0, 63)
        setChipPixel(127, 63)
        */
        
    }
    
    
    func setChipPixel(_ x:Int, _ y:Int) {
        var vramAddr = 0
        let rowBytes = 128
        vramAddr = (y * rowBytes) + x
        if Chip8VRAM[vramAddr] == 0 {
            Chip8VRAM[vramAddr] = 255
        } else {
            Chip8VRAM[vramAddr] = 0
            ChipRegister[15] = 1
        }
    }
    
    func getChipPixel(_ x:Int, _ y:Int) -> Int {
        var vramAddr = 0
        let rowBytes = 128
        vramAddr = (y * rowBytes) + x
        return Chip8VRAM[vramAddr]
    }
    
    
    func draw48Sprite(_ operand:UInt16){
        var high = 0
        var wide = 0
        let hiDef = Chip8.sharedInstance.resolution
        if hiDef==1 { high = 63 } else { high = 31 }
        if hiDef==1 { wide = 127 } else { wide = 63 }
        let x = ChipRegister[Int(operand>>8)] & wide //purpose of & is to clamp onscreen
        let y = ChipRegister[Int((operand>>4) & 0x0f)] & high
        var spriteAddr = IndexRegister
        var height = 16
        var row = y
        var currentScreenXPos = 0
        var srcBits = 0
        
        dsLog("DRW48 \(x),\(y),\(height)")

        if (height+y) > (high + 1) {
            height = (high+1)-y
        }
        while (height > 0) {
            srcBits = Int(Chip8RAM[spriteAddr])<<8
            srcBits += Int(Chip8RAM[spriteAddr+1])
            for theBit in 0...15 {
                currentScreenXPos = (x+15)-theBit & 127
                if (srcBits & (1 << theBit)) > 0 {
                    setChipPixel(currentScreenXPos, row)
                }
            }
            height -= 1
            row += 1
            spriteAddr+=2
        }
    }
    
    
    func drawSprite(_ operand:UInt16){
        var high = 31
        var wide = 63
        let hiDef = Chip8.sharedInstance.resolution
        if hiDef==1 { high = 63 } else { high = 31 }
        if hiDef==1 { wide = 127 } else { wide = 63 }
        let x = ChipRegister[Int(operand>>8)] & wide //purpose of & is to clamp onscreen
        let y = ChipRegister[Int((operand>>4) & 0x0f)] & high
        var spriteAddr = IndexRegister
        var height = Int(operand & 0x0f) & 0x0f
        var row = y
        var currentScreenXPos = 0
        var srcBits:Int
        dsLog("DRW \(x),\(y),\(height)")
        
        if (height+y) > (high + 1) {
            height = (high+1)-y
        }
        while (height > 0) {
            srcBits = Int(Chip8RAM[spriteAddr])
            currentScreenXPos = x
            while (srcBits > 0) {
                if (srcBits & 0b10000000) > 0 {
                    setChipPixel(currentScreenXPos, row)
                }
                srcBits = (srcBits << 1) & 0xff
                currentScreenXPos = (currentScreenXPos+1) & wide
            }
            height -= 1
            row += 1
            spriteAddr += 1
        }
    }
    
    
    func spriteFunction(_ operand:UInt16) {
        ChipRegister[15] = 0
        if operand & 0xf != 0 {
            drawSprite(operand)
        } else {
            draw48Sprite(operand)
        }
    }
    
    
    func storeBCD(_ decimal:Int) {
        var j:UInt8 = 0
        var bcd = decimal
        while(bcd >= 100) {
            Chip8RAM[IndexRegister] = j
            bcd = bcd - 100
            if j != 255 {
                j += 1
            } else {
                j=0
            }
        }
        j = 0
        while(bcd >= 10) {
            Chip8RAM[IndexRegister+1] = j
            bcd = bcd - 10
            if j != 255 {
                j += 1
            } else {
                j=0
            }
        }
        Chip8RAM[IndexRegister+2] = UInt8(bcd)
    }
    
    
    func waitKeyPress(_ register:UInt16,operand:UInt16) {
        //Skip Not Key Pressed
        dsLog("SNKP V\(register)")
        var cp_value=0
        if (operand & 0xff)==0x9e {
            cp_value=1
        } else {
            if (operand & 0xff)==0xa1 {
                cp_value=0
            } else {
                return
            }
        }
        let key_value=chip8_keys[Int(ChipRegister[Int(register)]) & 0x0f]
        if cp_value==Int(key_value) {
            ProgramCounter=ProgramCounter+2
        }

    }
    
    
    func dispatchSystemFunction(_ instruction:Int) {
        let sysFunc = instruction & 0xff
        switch sysFunc {
        case 0xe0:
            dsLog("CLS")
            for column in 0...8191 {
                Chip8VRAM[column] = 0
            }
        case 0xee:
            dsLog("RETURN")
            ProgramCounter = Int(Chip8RAM[StackPointer]) << 8
            StackPointer += 1
            ProgramCounter += Int(Chip8RAM[StackPointer])
            StackPointer += 1
        case 0xfd:
            //this is sort of like shutdown or exit(0)
            dsLog("EXIT")
            EmulationRunning = 0
        case 0xfe:
            dsLog("LORES")
            Chip8.sharedInstance.resolution = 0
        case 0xff:
            dsLog("HIRES")
            Chip8.sharedInstance.resolution = 1
        case 0xfb:
            dsLog("SCROLL RIGHT")
            scrollVRAMRight()
        case 0xfc:
            dsLog("SCROLL LEFT")
            scrollVRAMLeft()
        default:
            if instruction & 0xf0 == 0xc0 {
                dsLog("SCROLL DOWN")
                scrollVRAMDown()
            }
            //dsLog("Call to unknown system function: \(sysFunc)")
        }
    }
    
    
    func scrollVRAMRight() {
        let amount = 4
        var dstindex = 0
        var srcindex = 0
        for y in 0...63 {
            for x in (0...127).reversed() {
                if (x+amount) < 127 {
                    dstindex = (y * 128) + (x+amount)
                    srcindex = (y * 128) + x
                    Chip8VRAM[dstindex] = Chip8VRAM[srcindex]
                    Chip8VRAM[srcindex] = 0
                }
            }
        }
    }
    
    func scrollVRAMLeft() {
        let amount = 4
        var dstindex = 0
        var srcindex = 0
        for y in 0...63 {
            for x in 0...127 {
                if (x-amount) > 0 {
                    dstindex = (y * 128) + (x-amount)
                    srcindex = (y * 128) + x
                    Chip8VRAM[dstindex] = Chip8VRAM[srcindex]
                    Chip8VRAM[srcindex] = 0
                }
            }
        }
    }
    
    
    
    func scrollVRAMDown() {
        let amount = 4
        var srcindex = 0
        var dstindex = 0
        for y in(0...63).reversed() {
            for x in 0...127 {
                if (y+amount) < 63 {
                    dstindex = ((y+amount) * 128) + x
                    srcindex = (y * 128) + x
                    Chip8VRAM[dstindex] = Chip8VRAM[srcindex]
                    Chip8VRAM[srcindex] = 0
                }
            }
        }
    }
    
    
    func dispatchMathFunction(_ instruction:Int,register:Int,val:Int){
        var tmp = 0
        let value = Int(val)
        switch instruction {
        case 0:
            dsLog("LD \(value)->V\(register)")
            ChipRegister[register] = value
            ChipRegister[register] &= 0xff
        case 1:
            dsLog("OR V\(register),V\(value)")
            ChipRegister[register] = ChipRegister[register] | value//ChipRegister[Int(value)];
            ChipRegister[register] &= 0xff
        case 2:
            dsLog("AND V\(register),V\(value)")
            ChipRegister[register] = ChipRegister[register] & value//ChipRegister[Int(value)];
            ChipRegister[register] &= 0xff
        case 3:
            dsLog("XOR V\(register),V\(value)")
            ChipRegister[register] = ChipRegister[register] ^ value//ChipRegister[Int(value)];
            ChipRegister[register] &= 0xff
        case 4:
            dsLog("ADD V\(register),\(value)")
            tmp = ChipRegister[register] + value
            ChipRegister[register] = tmp
            ChipRegister[15] = tmp>>8
            ChipRegister[register] &= 0xff
            ChipRegister[15] &= 0xff
        case 5:
            dsLog("SUB V\(register),\(value)")
            tmp = ChipRegister[register] - value
            ChipRegister[register] = tmp
            ChipRegister[15] = (tmp>>8)+1
            ChipRegister[register] &= 0xff
            ChipRegister[15] &= 0xff
        case 6:
            dsLog("SR V\(register)")
            ChipRegister[15]=ChipRegister[register] & 1
            ChipRegister[register] = ChipRegister[register] >> 1
            ChipRegister[register] &= 0xff
            ChipRegister[15] &= 0xff
        case 7:
            dsLog("RSB \(register)")
            tmp = Int(value-ChipRegister[register])
            ChipRegister[register] = tmp
            ChipRegister[15] = (tmp>>8)+1
            ChipRegister[register] &= 0xff
            ChipRegister[15] &= 0xff
        case 8,9,10,11,12,13,15:
            dsLog("NOP - \(instruction)")
        case 14:
            dsLog("SL V\(register)")
            ChipRegister[15]=ChipRegister[register] >> 7
            ChipRegister[register] = ChipRegister[register] << 1
            ChipRegister[register] &= 0xff
            ChipRegister[15] &= 0xff
        default:
            dsLog("Unknown math function call \(instruction)")
        }
    }
    
    
    func dispatchExtended(_ operand:UInt16) {
        let extInstruction = operand & 0xff
        let register = operand >> 8
        switch extInstruction {
        case 0x07:
            dsLog("LD DT,V\(register)")
            ChipRegister[Int(register)] = DelayRegister
        case 0x0A:
            dsLog("WAITKEY \(register)")
            if Chip8KeyDown > 0 {
                ChipRegister[Int(register)] = Chip8KeyDown - 1
            } else {
                ProgramCounter-=2
            }
        case 0x15:
            dsLog("LD V\(register)->DT")
            DelayRegister = Int(ChipRegister[Int(register)])
            DelayRegister &= 0xffff
        case 0x18:
            dsLog("FM_START \(register)")
        case 0x1e:
            dsLog("ADD I,V\(register)")
            IndexRegister+=Int(ChipRegister[Int(register)])
            IndexRegister &= 0xffff
        case 0x29:
            //grabs one of the hex 0-9,A-F chars set in chip8 rom
            //this is a 5 byte sprite
            dsLog("LD I,F(V\(register))")
            IndexRegister=(Int(ChipRegister[Int(register)]) & 0x0f) * 5
            //IndexRegister &= 0xffff
        case 0x30:
            //loads font chars 0-9,A-F depending on register value, not implemented yet
            //this is a 10 byte sprite
            dsLog("LD I,F(SCHIP)(V\(register))")
            IndexRegister=(Int(ChipRegister[Int(register)]) & 0x0f) * 10
            //IndexRegister &= 0xffff
        case 0x33:
            dsLog("LD BCD, V\(register),")
            storeBCD(ChipRegister[Int(register)])
        case 0x55:
            let from = Int(operand>>8) & 0x0f
            dsLog("STORE V\(from)")
            for i in 0...from {
                Chip8RAM[IndexRegister+i] = UInt8(ChipRegister[i])
            }
        case 0x65:
            let from = Int(operand>>8) & 0x0f
            dsLog("LOAD V\(from)")
            for i in 0...from {
                ChipRegister[i] = Int(Chip8RAM[IndexRegister+i])
            }
        default:
            dsLog("Invalid ext. instruction: \(extInstruction)")
        }
    }
    
    
    func addImmediate (_ register:Int,value:Int) {
        dsLog("ADDI \(value),V\(register)")
        ChipRegister[register] += Int(value)
        ChipRegister[register] &= 0xff
    }
    
    func callAddr(_ operand:UInt16) {
        dsLog("CALL \(operand)")
        StackPointer -= 1
        Chip8RAM[StackPointer] = UInt8(ProgramCounter & 0xff)
        StackPointer -= 1
        Chip8RAM[StackPointer] = UInt8(ProgramCounter >> 8)
        ProgramCounter=Int(operand)
    }
    
    func skipEqual(_ operand:UInt16){
        let register = operand >> 8
        let value = operand & 0xff
        
        dsLog("SE V\(register),\(value)")
        
        if ChipRegister[Int(register)] == Int(value) {
            ProgramCounter += 2
        }
    }
    
    func skipRegistersEqual(_ operand:UInt16){
        let register = operand >> 8
        let register2 = (operand >> 4) & 0x0f
        
        dsLog("SE V\(register),V\(register2)")
        
        if ChipRegister[Int(register)] == ChipRegister[Int(register2)] {
            ProgramCounter += 2
        }
    }
    
    func skipNotEqual(_ operand:UInt16){
        let register = operand >> 8
        let value = operand & 0xff
        
        dsLog("SNE V\(register),\(value)")
        
        if ChipRegister[Int(register)] != Int(value) {
            ProgramCounter += 2
        }
    }
    
    func skipRegistersNotEqual(_ operand:UInt16){
        let register = operand >> 8
        let register2 = (operand >> 4) & 0x0f
        
        dsLog("SNE V\(register),V\(register2)")
        
        if ChipRegister[Int(register)] != ChipRegister[Int(register2)] {
            ProgramCounter += 2
        }
    }
    
    
    func cpuDispatch(_ instruction:Int,operand:UInt16) {
        var register:UInt16
        //var register2:UInt16
        var value:UInt16
        
        register = operand >> 8
        //register2 = (operand >> 4) & 0x0f
        value = operand & 0xff
        
        switch instruction {
        case 0:
            dispatchSystemFunction(Int(operand))
        case 1:
            dsLog("JP \(operand)")
            ProgramCounter = Int(operand)
        case 2:
            callAddr(operand)
        case 3:
            skipEqual(operand)
        case 4:
            skipNotEqual(operand)
        case 5:
            skipRegistersEqual(operand)
        case 6:
            dsLog("LD V\(register),\(value)")
            ChipRegister[Int(register)] = Int(value)
        case 7:
            addImmediate(Int(register),value:Int(value))
        case 8:
            dispatchMathFunction(Int(operand & 0x0f),register: Int(register),val:ChipRegister[Int((operand>>4) & 0x0f)])
        case 9:
            skipRegistersNotEqual(operand)
        case 10:
            dsLog("LD   I, \(operand)")
            IndexRegister = Int(operand)
        case 11:
            dsLog("JP V0")
            ProgramCounter = Int(operand) + Int(ChipRegister[0])
            ProgramCounter &= 0xffff
        case 12:
            dsLog("RND V\(register),\(operand & 0xff)")
            ChipRegister[Int(register)] = Int(UInt16(arc4random_uniform(255)) & (operand & 0xff))
        case 13:
            spriteFunction(operand)
        case 14:
            waitKeyPress(register,operand:operand)
        case 15:
            dispatchExtended(operand)
        default:
            dsLog("Invalid instruction: \(instruction)")
        }
    }
    
    
    func checkKeys() {
        let eventSource:CGEventSourceStateID = CGEventSourceStateID.combinedSessionState
        
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_1) { chip8_keys[1] = 1 } else { chip8_keys[1] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_2) { chip8_keys[2] = 1 } else { chip8_keys[2] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_3) { chip8_keys[3] = 1 } else { chip8_keys[3] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_4) { chip8_keys[12] = 1 } else { chip8_keys[12] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_Q) { chip8_keys[4] = 1 } else { chip8_keys[4] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_W) { chip8_keys[5] = 1 } else { chip8_keys[5] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_E) { chip8_keys[6] = 1 } else { chip8_keys[6] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_R) { chip8_keys[13] = 1 } else { chip8_keys[13] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_A) { chip8_keys[7] = 1 } else { chip8_keys[7] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_S) { chip8_keys[8] = 1 } else { chip8_keys[8] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_D) { chip8_keys[9] = 1 } else { chip8_keys[9] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_F) { chip8_keys[14] = 1 } else { chip8_keys[14] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_Z) { chip8_keys[10] = 1 } else { chip8_keys[10] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_X) { chip8_keys[0] = 1 } else { chip8_keys[0] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_C) { chip8_keys[11] = 1 } else { chip8_keys[11] = 0 }
        if CGEventSource.keyState(eventSource,key: kVK_ANSI_V) { chip8_keys[15] = 1 } else { chip8_keys[15] = 0 }
    }
    
    
    func fetchAndExecute () {
        //var go = true
        var opcode : UInt16 = 0
        var key_pressed = 0
        var operand : UInt16
        var instruction : Int

        //set go to false if so
        if EmulationRunning == 0 {
            return
        }
        var i = InterruptPeriod
        while i > 0 {
            i-=1
            opcode = UInt16(Chip8RAM[ProgramCounter])<<8
            opcode = opcode + UInt16(Chip8RAM[ProgramCounter+1])
            ProgramCounter += 2
            //dsLog("\(opcode)")
            //do core opcode dispatch
            instruction = Int(opcode >> 12)
            operand = UInt16(opcode & 0x0fff)
            cpuDispatch(instruction, operand:operand)
        }
        if DelayRegister > 0 {
            DelayRegister -= 1
        }
        if SoundRegister > 0 {
            SoundRegister -= 1
            if SoundRegister == 0 {
                //stop any sound
            }
        }
        //update display
        checkKeys()
        for i in 0...15 {
            if chip8_keys[i] > 0 {
                key_pressed = i + 1
            }
        }
        if (key_pressed > 0) && (key_pressed != Chip8KeyDown) {
            Chip8KeyDown = key_pressed
        } else {
            Chip8KeyDown = 0
        }
        //EmulationRunning--
        
    }
}
